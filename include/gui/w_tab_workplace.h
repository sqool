// License:   GPL 2 - se the COPYING file in the root directory

/*!
   The class manages tabbed workplace.
   @author  Marek Malevic
   @brief   Tabbed Workplace class
   @class   classTabWorkplace
   @date    2007
*/

#ifndef     H_TAB_WORKPLACE
#define     H_TAB_WORKPLACE

#include    <QTabWidget>

class classTabWorkplace : public QTabWidget
{

   Q_OBJECT

    public:
         /*!
            creates the tabbed workplace
            @sa      ~classDBObjectTree()
         */
                        classTabWorkplace();
         /*!
            destroys the tabbed workplace
            @sa      classDBObjectTree()
         */
        virtual        ~classTabWorkplace();

};

#endif


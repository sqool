// License:   GPL 2 - se the COPYING file in the root directory

/*!
   The class manages all the main  window gui things.
   @author  Marek Malevic
   @brief   Main window GUI class
   @class   classMainForm
   @date    2006
*/

#ifndef     H_FRMMAIN
#define     H_FRMMAIN

#include       <QMainWindow>
#include       <QDockWidget>
#include       <QMenu>
#include       <QMenuBar>
#include       "w_tab_workplace.h"
#include       "w_object_tree.h"
#include       "frm_reg.h"

/* minimum window size */
#define        FRMMAIN_UI_WMINW     640
#define        FRMMAIN_UI_WMINH     480


class classMainForm : public QMainWindow
{
    Q_OBJECT
            
    public:
         /*!
            creates application main window
            @sa      ~classMainForm()
         */
                        classMainForm();
         /*!
            destroys application main window
            @sa      classMainForm()
         */
         virtual        ~classMainForm();

         /*!
            Database object tree
            @sa      classDBObjectTree();
         */
         classDBObjectTree
                       *dbOTree;

         /*!
            gets the window state code
            @sa      setWindowStateInt();
         */
         unsigned short int
                        getWindowStateInt();

         /*!
            sets the window state
            @sa      getWindowStateInt();
         */
         void           setWindowStateInt
                           (const int i_winstate);

     private:

         /*!
            Tabbed workplace (central widget)
            @sa      classTabWorkplace();
         */
         classTabWorkplace
                       *tabWorkplace;

         /*!
            create the actions
            @sa      classMainForm();
         */
         void           createActions();

         /*!
            create the window's main menu
            @sa      classMainForm();
         */
         void           createMenus();

         // menus
         /*!
            Window main menu - File
         */
         QMenu
                        *fileMenu;
         /*!
            Window main menu - Database
         */
         QMenu
                        *databaseMenu;

         /*!
            Db registration dialog
         */
         classRegForm   *frmReg;

      public slots:
         
         /*!
            Connects selected db
         */
         void connectDb();

         /*!
            Disconnects selected db
         */
         void disconnectDb();

         /*!
            Opens the register db dialog
         */
         void registerDb();

         /*!
            Asks and unregisters the db
         */
         void unregisterDb();

         /*!
            Enables/disables actions according
            to the application state.
         */
         void checkActions();

};

#endif
/*
#include       <QMainWindow>
#include       <QWorkspace>
#include        <QTreeWidget>
#include        <QTreeWidgetItem>
#include        <QTreeWidgetItemIterator>
#include <QFile>
#include        <QDockWidget>
#include        <QPalette>
#include        <QMenu>
#include        <QMenuBar>
#include        <QToolBar>
#include        <QComboBox>
#include        <QList>
#include        <QPushButton>
#include        <QContextMenuEvent>

#include "appl.h"
#include "configuration.h"
#include        "frmreg.h"
#include        "tabquery.h"
*/
/*
item types (hidden in the second column)
*/
//root - registered dbs under this
/*
#define IT_CATEGORY_DB              0x0
#define IT_DB                       0x1
#define IT_CATEGORY_TABLE           0x2
#define IT_TABLE                    0x3
#define IT_CATEGORY_INDEX           0x4
#define IT_INDEX                    0x5
#define IT_CATEGORY_FIELD           0x6
#define IT_FIELD                    0x7


class QAction;
class QMenu;

class CMainForm : public QMainWindow
{
    Q_OBJECT
            
    public:
                   CMainForm();
        virtual         ~CMainForm();
        
    public slots:
        void            slotReloadRegisteredDBs();
        void            slotCheckMenusEnabled();
        
    private:
        QTreeWidget     *dbTree;
        QTreeWidgetItem *rdb;
        QTabWidget      *TabWorkplace;
        CRegForm        *frmReg;
        
        QMenu           *MenuFile;
        
        QAction         *ActConnectDB;
        QAction         *ActDisconnectDB;
        QAction         *ActRegDB;
        QAction         *ActUnRegDB;
        QAction         *ActExit;
        QAction         *ActNewQuery;
        QAction         *ActRunQuery;
        QAction         *ActReloadRegDB;
        QAction         *ActCloseTab;
        QAction         *ActOpenQuery;
        QAction         *ActSaveQuery;
        QAction         *ActSaveAsQuery;
        QAction         *ActOpenTableView;
        
        QAction         *ActScriptSelect;
        QAction         *ActScriptCreate;
        QAction         *ActScriptDrop;
                
        QToolBar        *ToolBarQueryEditor;
        QToolBar        *ToolBarDB;
        QComboBox       *RegCombo;
        
        void            AddDB(const char* str);
        void            RegDBDialog();
        int             GetItemType(QTreeWidgetItem *itm);
        QTreeWidgetItem *GetRegDBsItem();
        QTreeWidgetItem *GetSelectedItem();
        bool            IsChildOfItem(QTreeWidgetItem *child, QTreeWidgetItem *parent);
        void            UpdateRegCombo();
        
        void            ConnectDB(bool bWarnOnNoFile, QTreeWidgetItem *si);
        void            DisconnectDB(QTreeWidgetItem *si);
        
        void            LoadTables(QTreeWidgetItem *parent, QStringList tbls);
        void            LoadIndices(QTreeWidgetItem *parent, QStringList indcs);                
        void            CreateActions();
        void            CreateDockWindows();
        void            CreateMenus();
        void            CreateToolBars();
        void            CheckEnabledMenus();
        void            ShowTreeWidgetActions(const QPoint &pos);
        
        unsigned short int
                        GetWindowStateInt();
        void            SetWindowStateInt(const int i_winstate = 0);
        
        bool            IsCurrentWidgetTabQueryAndHasCode();
        bool            IsCurrentWidgetTabQuery();
        bool            IsCurrentWidgetSaved();
        bool            CurrentTabSave();
        bool            CurrentTabSaveAs();
    
    private slots:
        void            slotRegDBDialog();
        void            slotUnRegDB();
        void            slotConnectDB();
        void            slotConnectDBonDblClick(QTreeWidgetItem * item, int column );
        void            slotDisconnectDB();
        void            slotTreeWidgetClicked();
        void            slotTreeWidgetRightClicked(const QPoint &pos);
        void            slotNewQuery();
        void            slotTabChanged();
        void            slotComboDBChanged(QString sDB);
        void            slotExecuteQuery();
        void            slotCloseCurrentTab();
        void            slotSaveQuery();
        void            slotSaveAsQuery();
        void            slotOpenQuery();
        void            slotOpenTableView();
        void            slotScriptSelect();
        void            slotScriptCreate();
        void            slotScriptDrop();
        
    signals:
        void            signalReloadRegisteredDBs();
        
};

#endif
*/

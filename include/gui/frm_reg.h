#ifndef H_FRMREG
#define H_FRMREG

#include        <QWidget>
#include        <QFileDialog>
#include        <QPushButton>
#include        <QLineEdit>
#include        <QLabel>
#include        <QMessageBox>
#include        <iostream>

#define        FRM_REG_WIDTH   600
#define        FRM_REG_HEIGHT  200

#define        FRM_REG_SIZE_CONSTS_TOP             10
#define        FRM_REG_SIZE_CONSTS_LEFT            15
#define        FRM_REG_SIZE_CONSTS_BUTTON_WIDTH    90

//#include        "configuration.h"

class classRegForm : public QWidget
{
    Q_OBJECT
            
    public:
                        classRegForm(QWidget *parent = 0);
        virtual         ~classRegForm();
/*
    signals:
        void            signalReloadRegisteredDBs();    
*/        
    private:
        QFileDialog     *file_dialog;
        QPushButton     *button_select_db;
        QPushButton     *button_ok;
        QPushButton     *button_cancel;
        QLineEdit       *db_path;
        QLabel          *path_label;
        QLineEdit       *db_title;
        QLabel          *title_label;
        
        void            connectFileDialog();
    
    private slots:
        void            fillPath();
        void            regDBOK();
        void            checkButtons();      

};

#endif

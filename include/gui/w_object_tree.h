// License:   GPL 2 - se the COPYING file in the root directory

/*!
   The class manages the database object tree widget.
   @author  Marek Malevic
   @brief   Database Object Tree class
   @class   classDBObjectTree
   @date    2006-2007
*/

#ifndef     H_DBOBJECT_TREE
#define     H_DBOBJECT_TREE

#include    <QTreeWidget>
#include    "../core/object_tree_model.h"

class classDBObjectTree : public QTreeWidget
{

   Q_OBJECT

    public:
         /*!
            creates the db object tree
            @sa      ~classDBObjectTree()
         */
                        classDBObjectTree();
         /*!
            destroys the db object tree
            @sa      classDBObjectTree()
         */
        virtual        ~classDBObjectTree();

         /*!
            @sa      classDBObjectTreeModel()
         */
        classDBObjectTreeModel
                       *model;
   
         /*!
            returns ID of the selected object
         */         
        int             selectedObjectID();

         /*!
            redraws the tree according to the model object
            @sa      classDBObjectTree()
         */
        int             redrawTree();


    private:
         
         /*!
            draw object
            @param   object_id model id of the object
            @param   parent object to draw the kid under
            @param   kid returns the kid object
         */
        int             drawObject(int object_id, QTreeWidgetItem *parent, QTreeWidgetItem *&kid,classQuery &qdb);

         /*!
            draws all objects under the object_id object found in object model db
            @param   object_id model id of the starting object
            @param   parent object to draw the kids under
         */
        int             drawKids(int object_id, QTreeWidgetItem *parent,classQuery &qdb);

};

#endif


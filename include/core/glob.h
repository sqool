// License:   GPL 2 - se the COPYING file in the root directory

/*!
   @author  Marek Malevic
   @brief   General application constants
   @date    2007
*/


#ifndef     H_GLOB
#define     H_GLOB

#define     APP_NAME          "Sqool"
#define     APP_VERSION       "0.0.0"

#define     APP_CONFIG_PATH   "sqool"

/* Global sqool constants */
#define     CONST_OK        0
#define     CONST_ERROR     -1

/* tree object types */

   /* relational db root */
#define   IT_ROOT_DB        1000 + 0x001
   /* reserved for sqlite olap system */
#define   IT_ROOT_OLAP      1000 + 0x002
   /* relational db */
#define   IT_DB             1000 + 0x011

#define   NOT_EXPANDED      0
#define   EXPANDED          1
#define   NOT_CONNECTED     0
#define   CONNECTED         1


/* database types */
#define   DT_NONE          0
#define   DT_DB            1
#define   DT_OLAP          2

/* DB registration results */
#define   RDB_OK                      CONST_OK
#define   RDB_DB_REGISTERED           3
#define   RDB_TITLE_USED              4

#endif

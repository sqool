// License:   GPL 2 - se the COPYING file in the root directory

/*!
   @author  Marek Malevic
   @brief   General application functions
   @date    2006-2007
*/


#ifndef		H_APPL
#define		H_APPL

/* it's a Qt application */
#include    <QApplication>

/* returns the config file location */
std::string       configurationFile();

/* Creates/checks the configuration path.
If successful returns CONST_OK */
const int         checkConfigurationPath();
/* Creates/checks the config db structure*/
const int         checkConfigurationStructure();

/* Gets the configuration value. If there is no record it returns the default_value. */
const std::string configGetValue(const std::string name, const std::string default_value); 
const int configGetValue(const std::string name, const int default_value); 

/* Sets the configuration value. If succesfull returns CONST_OK*/
const int configSetValue(const std::string name, const std::string value); 
const int configSetValue(const std::string name, const int value);

#endif

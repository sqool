// License:   GPL 2 - se the COPYING file in the root directory

/*!
   The class manages the standard output things.
   @author  Marek Malevic
   @brief   Stdout wrapper
   @class   classStdoutput
   @date    2006
*/

#ifndef     H_STDOUTPUT
#define     H_STDOUTPUT

/*!
   @enum VER_MODE Defines the verbosity steps.
*/
typedef enum {
/* no output */
   V_MODE_MUTE       = 0,
/* errors */
   V_MODE_ERRORS     = 1,
/* + warnings */
   V_MODE_WARNINGS   = 2,
/* + advices */
   V_MODE_ADVICE     = 3,
/* + info */
   V_MODE_INFO       = 4
} VER_MODE ;


#define     V_DEFAULT_MODE       V_MODE_INFO

#define     STRING_ADVICE        "ADVICE:"
#define     STRING_ERROR         "ERROR:"
#define     STRING_INFO          ""
#define     STRING_WARNING       "WARNING:"

#include    <iostream>

class classStdoutput{
public:
   /*!
      creates the stdoutput object
      @param   v_mode the verbosity mode
      @sa      ~classStdoutput()
   */
   classStdoutput(VER_MODE v_mode = V_DEFAULT_MODE);

   /*!
      destroys the db engine object
      @sa      classStdoutput(VER_MODE v_mode = V_DEFAULT_MODE)
   */
   ~classStdoutput();

   /*!
      sends a message to std::cout if the verbosity mode is at least V_MODE_ADVICE
      @param   s_message message to send
      @sa      classStdoutput(VER_MODE v_mode = V_DEFAULT_MODE),
               Error(std::string s_message, int erro_code = 0),
               Warning(std::string s_message),
               Info(std::string s_message)
   */   
   void           Advice(std::string s_message, bool show_msgbox = 0);

   /*!
      sends a message to std::cerr if the verbosity mode is at least V_MODE_ERROR
      @param   s_message message to send
      @param   erro_code error number
      @sa      classStdoutput(VER_MODE v_mode = V_DEFAULT_MODE),
               Warning(std::string s_message),
               Advice(std::string s_message),
               Info(std::string s_message)
   */
   void           Error(std::string s_message, bool show_msgbox = 0);

   /*!
      sends a message to std::cout if the verbosity mode is at least V_MODE_INFO
      @param   s_message message to send
      @sa      classStdoutput(VER_MODE v_mode = V_DEFAULT_MODE),
               Error(std::string s_message, int erro_code = 0),
               Warning(std::string s_message),
               Advice(std::string s_message),
   */
   void           Info(std::string s_message, bool show_msgbox = 0);

   /*!
      sends a message to std::clog if the verbosity mode is at least V_MODE_WARNING
      @param   s_message message to send
      @sa      classStdoutput(VER_MODE v_mode = V_DEFAULT_MODE),
               Error(std::string s_message, int erro_code = 0),
               Advice(std::string s_message),
               Info(std::string s_message)
   */
   void           Warning(std::string s_message, bool show_msgbox = 0);

private:
   unsigned int   verbosity_mode;
};


#endif

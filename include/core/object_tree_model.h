// License:   GPL 2 - se the COPYING file in the root directory

/*!
   The class manages the database object tree model.
   @author  Marek Malevic
   @brief   Database Object Tree Item class
   @class   classDBObjectTreeModel
   @date    2007
*/

#ifndef     H_DBOBJECT_TREE_MODEL
#define     H_DBOBJECT_TREE_MODEL

#include       <list>
#include       "database.h"
#include       "query.h"


class classDBObjectTreeModel
{

    public:
         /*!
            creates the db object model item
            @sa      ~classDBObjectTreeModel()
         */
                        classDBObjectTreeModel();
         /*!
            destroys the db object model item
            @sa      classDBObjectTreeModel()
         */
        virtual        ~classDBObjectTreeModel();


         /*!
            returns the list of object children ids sorted by name
         */
         int            getKids(
                              const int parent_id,
                              std::list<int> &li,
                              classQuery &qdb
                           );

         /*!
            get the object info
            @sa      objectID()
         */
         int objectInfo(
                  int object_id, uint &object_type,
                  int &parent_id, std::string &caption,
                  uint &expanded, int &db_type, std::string &parental_db,
                  classQuery &qdb
               );

         int
            objectInfo(
                  int object_id, uint &object_type,
                  int &parent_id, std::string &caption,
                  uint &expanded, int &db_type, std::string &parental_db,
                  std::string &db_path, std::string &title, int &connected,
                  classQuery &qdb
            );

         /*!
            Returns or sets the db connection state.
            @sa      objectInfo()
         */
         int   dbConnected(
                  int db_type, std::string db_path, classQuery &qdb
            );
         int   dbConnected(
                  const int i_connected, int db_type, std::string db_path, classQuery &qdb
            );

         /*!
            registers the sqlite db
            @sa      unregisterDB
         */
         int 
             registerDB(
               std::string s_path, std::string s_title,
               std::string &s_path_used, std::string &s_title_used,
               const int db_type, classQuery &qdb
            );

         /*!
            unregisters the sqlite db
            @sa      registerDB
         */
         int 
            unregisterDB(
               std::string s_path, const int db_type, classQuery &qdb
            );

    private:

         /*!
            adds an object to the model tree
         */
         void addObject(
                  const uint object_type, const int parent_id,
                  const std::string caption, const uint expanded,
                  std::string parental_db, int db_type, classQuery &qdb
               );
   /*      void addObject(const uint object_type, const int parent_id,
                  const uint expanded,classQuery &qdb
               );
*/
         /*!
            Gets the object id for given object type.
            The first object's id of such type is returned.
            If there is no such object, CONST_ERROR is returned.
            @sa      objectInfo()
         */
         int  objectTypeToID( const uint object_type ,classQuery &qdb );

};

#endif


#ifndef _DATABASE_H_SQLITE
#define _DATABASE_H_SQLITE

#include <pthread.h>
#include <string>
#include <list>
#include <stdint.h>
#include <sqlite3.h>

#ifdef SQLITEW_NAMESPACE
namespace SQLITEW_NAMESPACE {
#endif


class classQuery;
class Mutex;


/** Connection information and pool. */
class classDatabase 
{
public:
	/** Mutex container class, used by Lock. 
		\ingroup threading */
	class Mutex {
	public:
		Mutex();
		~Mutex();
		void Lock();
		void Unlock();
	private:
		pthread_mutex_t m_mutex;
	};
private:
	/** Mutex helper class. */
	class Lock {
	public:
		Lock(Mutex& mutex,bool use);
		~Lock();
	private:
		Mutex& m_mutex;
		bool m_b_use;
	};
public:
	/** Connection pool struct. */
	struct OPENDB {
		OPENDB() : busy(false) {}
		sqlite3 *db;
		bool busy;
	};
	typedef std::list<OPENDB *> opendb_v;

public:
	/** Use file */
	classDatabase(const std::string& database);

	/** Use file + thread safe */
	classDatabase(Mutex& ,const std::string& database);

	virtual ~classDatabase();

	/** try to establish connection with given host */
	bool Connected();

	void error(classQuery&,const char *format, ...);
	void error(classQuery&,const std::string& );

	/** Request a database connection.
The "grabdb" method is used by the classQuery class, so that each object instance of classQuery gets a unique
database connection. I will reimplement your connection check logic in the classQuery class, as that's where
the database connection is really used.
It should be used something like this.
{
		classQuery q(db);
		if (!q.Connected())
			 return false;
		q.execute("delete * from user"); // well maybe not
}

When the classQuery object is deleted, then "freedb" is called - the database connection stays open in the
m_opendbs vector. New classQuery objects can then reuse old connections.
	*/
	OPENDB *grabdb();
	void freedb(OPENDB *odb);

	/** Escape string - change all ' to ''. */
	std::string safestr(const std::string& );
	/** Make string xml safe. */
	std::string xmlsafestr(const std::string& );

	/** Convert string to 64-bit integer. */
	int64_t a2bigint(const std::string& );
	/** Convert string to unsigned 64-bit integer. */
	uint64_t a2ubigint(const std::string& );

private:
	classDatabase(const classDatabase& ) : m_mutex(m_mutex) {}
	classDatabase& operator=(const classDatabase& ) { return *this; }
	void error(const char *format, ...);
	//
	std::string database;
	opendb_v m_opendbs;
	bool m_embedded;
	Mutex& m_mutex;
	bool m_b_use_mutex;
};


#ifdef SQLITEW_NAMESPACE
} // namespace SQLITEW_NAMESPACE {
#endif

#endif // _DATABASE_H

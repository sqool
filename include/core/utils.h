// License:   GPL 2 - se the COPYING file in the root directory

/*!
   @author  Marek Malevic
   @brief   Useful not only conversion functions
   @date    2007
*/


#ifndef     H_UTILS
#define     H_UTILS

#include    <sstream>

std::string toString(const int value);


#endif

// License:   GPL 2 - se the COPYING file in the root directory

/*!
   @author  Marek Malevic
   @brief   Application actions
   @date    2007
*/


#ifndef		H_ACTIONS
#define		H_ACTIONS

#include    <QAction>

QAction         *actExit;

QAction         *actRegisterDB;
QAction         *actUnRegisterDB;
QAction         *actConnectDB;
QAction         *actDisConnectDB;

#endif

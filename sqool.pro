HEADERS += include/core/appl.h \
           include/core/glob.h \
	   include/core/utils.h \
	   include/core/actions.h \
	   include/core/object_tree_model.h \
           include/core/database.h \
	   include/core/query.h \
           include/core/stdoutput.h \
	   include/gui/frm_main.h \
	   include/gui/frm_reg.h \
	   include/gui/w_object_tree.h \
	   include/gui/w_tab_workplace.h \
	   include/gui/gui.h

SOURCES += src/core/appl.cpp \
	   src/core/utils.cpp \
	   src/core/main.cpp \
           src/core/stdoutput.cpp \
           src/core/object_tree_model.cpp \
	   src/core/database.cpp \
	   src/core/query.cpp \
	   src/gui/frm_main.cpp \
	   src/gui/frm_reg.cpp \
	   src/gui/w_object_tree.cpp \
	   src/gui/w_tab_workplace.cpp

TARGET = sqool
TARGET.files = sqool

TARGET.path = /usr/bin

LIBS += -lsqlite3

RESOURCES += icons/icons.qrc

INSTALLS += TARGET

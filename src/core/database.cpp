#include <stdio.h>

#include <string>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include       "../../include/core/database.h"
#include       "../../include/core/query.h"
#include       "../../include/core/stdoutput.h"

/* joins the standard output machine */
extern         classStdoutput::classStdoutput say;


#ifdef SQLITEW_NAMESPACE
namespace SQLITEW_NAMESPACE {
#endif


classDatabase::classDatabase(const std::string& d)
:database(d)
,m_embedded(true)
,m_mutex(m_mutex)
,m_b_use_mutex(false)
{
}


classDatabase::classDatabase(Mutex& m,const std::string& d)
:database(d)
,m_embedded(true)
,m_mutex(m)
,m_b_use_mutex(true)
{
}


classDatabase::~classDatabase()
{
	for (opendb_v::iterator it = m_opendbs.begin(); it != m_opendbs.end(); it++)
	{
		OPENDB *p = *it;
		sqlite3_close(p -> db);
	}
	while (m_opendbs.size())
	{
		opendb_v::iterator it = m_opendbs.begin();
		OPENDB *p = *it;
		if (p -> busy)
		{
			error("destroying classDatabase object before classQuery object");
		}
		delete p;
		m_opendbs.erase(it);
	}
}


classDatabase::OPENDB *classDatabase::grabdb()
{
	Lock lck(m_mutex, m_b_use_mutex);
	OPENDB *odb = NULL;

	for (opendb_v::iterator it = m_opendbs.begin(); it != m_opendbs.end(); it++)
	{
		odb = *it;
		if (!odb -> busy)
		{
			break;
		}
		else
		{
			odb = NULL;
		}
	}
	if (!odb)
	{
		odb = new OPENDB;
		if (!odb)
		{
			error("grabdb: OPENDB struct couldn't be created");
			return NULL;
		}
		int rc = sqlite3_open(database.c_str(), &odb -> db);
		if (rc)
		{
			error("Can't open database: %s\n", sqlite3_errmsg(odb -> db));
			sqlite3_close(odb -> db);
			delete odb;
			return NULL;
		}
		odb -> busy = true;
		m_opendbs.push_back(odb);
	}
	else
	{
		odb -> busy = true;
	}
	return odb;
}


void classDatabase::freedb(classDatabase::OPENDB *odb)
{
	Lock lck(m_mutex, m_b_use_mutex);
	if (odb)
	{
		odb -> busy = false;
	}
}


void classDatabase::error(const char *format, ...)
{
   va_list ap;
	char errstr[5000];
	va_start(ap, format);
	vsnprintf(errstr, 5000, format, ap);
	va_end(ap);
   say.Error(errstr);
}


void classDatabase::error(classQuery& q,const char *format, ...)
{

   va_list ap;
   char errstr[5000];
   va_start(ap, format);
   vsnprintf(errstr, 5000, format, ap);
   va_end(ap);
   say.Error(errstr);
   say.Error("QUERY: \"" + q.GetLastQuery() + "\"");  
}


void classDatabase::error(classQuery& q,const std::string& msg)
{
   say.Error(msg);
   say.Error("QUERY: \"" + q.GetLastQuery() + "\"");  
}


bool classDatabase::Connected()
{
	OPENDB *odb = grabdb();
	if (!odb)
	{
		return false;
	}
	freedb(odb);
	return true;
}


classDatabase::Lock::Lock(Mutex& mutex,bool use) : m_mutex(mutex),m_b_use(use) 
{
	if (m_b_use) 
	{
		m_mutex.Lock();
	}
}


classDatabase::Lock::~Lock() 
{
	if (m_b_use) 
	{
		m_mutex.Unlock();
	}
}


classDatabase::Mutex::Mutex()
{
	pthread_mutex_init(&m_mutex, NULL);
}


classDatabase::Mutex::~Mutex()
{
	pthread_mutex_destroy(&m_mutex);
}


void classDatabase::Mutex::Lock()
{
	pthread_mutex_lock(&m_mutex);
}


void classDatabase::Mutex::Unlock()
{
	pthread_mutex_unlock(&m_mutex);
}


std::string classDatabase::safestr(const std::string& str)
{
	std::string str2;
	for (size_t i = 0; i < str.size(); i++)
	{
		switch (str[i])
		{
		case '\'':
		case '\\':
		case 34:
			str2 += '\'';
		default:
			str2 += str[i];
		}
	}
	return str2;
}


std::string classDatabase::xmlsafestr(const std::string& str)
{
	std::string str2;
	for (size_t i = 0; i < str.size(); i++)
	{
		switch (str[i])
		{
		case '&':
			str2 += "&amp;";
			break;
		case '<':
			str2 += "&lt;";
			break;
		case '>':
			str2 += "&gt;";
			break;
		case '"':
			str2 += "&quot;";
			break;
		case '\'':
			str2 += "&apos;";
			break;
		default:
			str2 += str[i];
		}
	}
	return str2;
}


int64_t classDatabase::a2bigint(const std::string& str)
{
	int64_t val = 0;
	bool sign = false;
	size_t i = 0;
	if (str[i] == '-')
	{
		sign = true;
		i++;
	}
	for (; i < str.size(); i++)
	{
		val = val * 10 + (str[i] - 48);
	}
	return sign ? -val : val;
}


uint64_t classDatabase::a2ubigint(const std::string& str)
{
	uint64_t val = 0;
	for (size_t i = 0; i < str.size(); i++)
	{
		val = val * 10 + (str[i] - 48);
	}
	return val;
}


#ifdef SQLITEW_NAMESPACE
} // namespace SQLITEW_NAMESPACE {
#endif


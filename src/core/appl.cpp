/*
    Author: Marek Malevic
    License:   GPL 2
*/

#include       <QDir>
#include       <QFSFileEngine>
#include       <sstream>
#include       "../../include/core/appl.h"
#include       "../../include/core/database.h"
#include       "../../include/core/query.h"
#include       "../../include/core/stdoutput.h"
#include       "../../include/core/utils.h"
#include       "../../include/core/glob.h"

/* joins the standard output machine */
extern         classStdoutput::classStdoutput say;

std::string
   configurationFile()
{
    return QFSFileEngine::homePath().toStdString() + "/.config/" + APP_CONFIG_PATH + "/configuration_ng";
}

const int
   checkConfigurationPath()
{
   QDir    fs;
   QString qs_ConfigPath(QFSFileEngine::homePath() + "/.config/" + APP_CONFIG_PATH);
   return fs.mkpath(qs_ConfigPath) ? CONST_OK : CONST_ERROR;
}

const int
   checkConfigurationStructure()
{
   classDatabase cdb(configurationFile());

   if (!cdb.Connected()) {
      return CONST_ERROR;
   };

   classQuery qdb(cdb);

   std::string s_sql = ""  
                    "CREATE TABLE IF NOT EXISTS"
                    "   t_config_records ("
                    "   name TEXT PRIMARY KEY,"
                    "   value TEXT"
                    ")";
   if (!qdb.execute(s_sql)) {
      say.Error("The configuration table could not be created.");
      return CONST_ERROR;
   };


   s_sql = ""
                 "CREATE TABLE IF NOT EXISTS"
                 "   t_registered_dbs ("
                 "   db_path TEXT PRIMARY KEY,"
                 "   db_type INTEGER DEFAULT " + toString(DT_DB) + ","
                 "   title TEXT,"
                 "   connected INTEGER DEFAULT 0"
                 ")";
   if (!qdb.execute(s_sql)) {
            say.Error("The db registrator table could not be created.");
      return CONST_ERROR;
   };


   s_sql = ""  
                      "CREATE TABLE IF NOT EXISTS"
                      "   t_tree_model ("
                      "   object_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                      "   object_type INTEGER,"
                      "   parent_id INTEGER,"
                      "   parental_db TEXT,"
                      "   db_type INTEGER,"
                      "   caption TEXT,"
                      "   expanded INTEGER"
                      ")";
   if (!qdb.execute(s_sql)) {
      say.Error("The object tree model table could not be created.");
      return CONST_ERROR;
   };


   s_sql = "SELECT COUNT(*) FROM t_tree_model WHERE object_type = " + toString(IT_ROOT_DB) + " ";
   if (qdb.get_count(s_sql)==0) {
   
         s_sql =  ""  
                              "INSERT INTO "
                              "   t_tree_model ("
                              "   object_type,"
                              "   parent_id,"
                              "   expanded, "
                              "   db_type "
                              ") VALUES ( " + toString(IT_ROOT_DB) + " "
                              ",  -1, "
                              "  " + toString(EXPANDED) + ","
                              "  " + toString(DT_NONE) + " )";
   
         if (!qdb.execute(s_sql)) {
            say.Error("The initial model tree object record could not be inserted.");
            return CONST_ERROR;
         };
   };

   return CONST_OK;

}


/* Gets the configuration value. If there is no record it returns the default_value. */
const std::string
   configGetValue(const std::string name, const std::string default_value)
{
   std::string retval = default_value;
  
   classDatabase cdb(configurationFile());

   if (cdb.Connected()) {
      classQuery qdb(cdb);
   
      std::string s_sql = ""  
                     "SELECT "
                     "   value "
                     "FROM t_config_records "
                     "WHERE "
                     "   name = '" + name + "'";   
   
      qdb.get_result(s_sql);
   
      while (qdb.fetch_row()) {
         retval = qdb.getstr();
      }

      qdb.free_result();
   };

   return retval;
} 

const int
   configGetValue(const std::string name, const int default_value)
{
   int retval = default_value;
  
   classDatabase cdb(configurationFile());

   if (cdb.Connected()) {
      classQuery qdb(cdb);
   
      std::string s_sql = ""  
                     "SELECT "
                     "   value "
                     "FROM t_config_records "
                     "WHERE "
                     "   name = '" + name + "'";   
   
      qdb.get_result(s_sql);
   
      while (qdb.fetch_row()) {
         retval = qdb.getval();
      }
      qdb.free_result();
   };

   return retval;
} 


/* Sets the configuration value. If succesfull returns CONST_OK*/
const int 
   configSetValue(const std::string name, const std::string value)
{
   classDatabase cdb(configurationFile());
   if (!cdb.Connected()) {
      return CONST_ERROR;
   };

   classQuery qdb(cdb);
   if (!qdb.execute("BEGIN TRANSACTION")) {
      say.Error("Cannot start new transaction. No configuration value update is going to be done.");
      return CONST_ERROR;
   };   

      std::string s_sql = ""  
                     "DELETE "
                     "FROM t_config_records "
                     "WHERE "
                     "   name = '" + name + "'";
      if (!qdb.execute(s_sql)) {
            say.Warning("Cannot clean the previous configuration value.");
      };   

      s_sql = "INSERT INTO t_config_records VALUES ('" + name + "', '" + value + "')";   
      if (!qdb.execute(s_sql)) {
            say.Error("Cannot update the configuration value.");
      };   

   if (qdb.execute("COMMIT TRANSACTION")) {
      say.Info(name + " value has been updated");
      return CONST_OK;
   } else {
      say.Error("Cannot update the configuration value.");
      return CONST_ERROR;
   };

} 

const int 
   configSetValue(const std::string name, const int value)
{
   return configSetValue(name, toString(value));
} 


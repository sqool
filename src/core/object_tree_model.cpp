/*
    Author: Marek Malevic
    License:   GPL 2
*/

#include       <sstream>

#include       "../../include/core/appl.h"
#include       "../../include/core/object_tree_model.h"
#include       "../../include/core/stdoutput.h"
#include       "../../include/core/utils.h"
#include       "../../include/core/glob.h"
#include       "../../include/core/query.h"  

/* joins the standard output machine */
extern         classStdoutput::classStdoutput say;

classDBObjectTreeModel::classDBObjectTreeModel()
{

   say.Info("Object tree model has been initialized.");
}


classDBObjectTreeModel::~classDBObjectTreeModel()
{
   
   say.Info("Object tree model has been destroyed.");

}


void
   classDBObjectTreeModel::addObject(
      const uint object_type, const int parent_id,
      const std::string caption, const uint expanded,
      const std::string parental_db, int db_type, classQuery &qdb
   )
{   
   std::string s_sql = ""  
                      "INSERT INTO"
                      "   t_tree_model( "
                      "      object_type, parent_id, caption, expanded, parental_db, db_type) "
                      "VALUES ( "
                      " " + toString(object_type) + ","
                      " " + toString(parent_id) + ","
                      " '" + caption + "',"
                      " " + toString(expanded) + ","
                      " '" + parental_db + "',"
                      " " + toString(db_type) + ""
                      " )";
   if (!qdb.execute(s_sql)) {
      say.Error("Object tree model object could not be added.");
   };

}

/*
void
   classDBObjectTreeModel::addObject(
      const uint object_type, const int parent_id,
      const uint expanded, classQuery &qdb
   )
{
   addObject(object_type, parent_id, "", expanded, "", qdb);
}
*/

int
   classDBObjectTreeModel::getKids(
      const int parent_id,
      std::list<int> &li,
      classQuery &qdb
   )
{

   std::string s_sql = "SELECT object_id FROM t_tree_model "
                       "WHERE parent_id = " + toString(parent_id) + " "
                       "ORDER BY caption";

   if (qdb.get_result(s_sql))
   {
         while (qdb.fetch_row()) {
            li.push_back(qdb.getval());
         }
         qdb.free_result();
   } else {
         say.Info("Unable to read object kids.");
         return CONST_ERROR;      
   };
   
   return CONST_OK;
}

int
   classDBObjectTreeModel::objectInfo(
         int object_id, uint &object_type,
         int &parent_id, std::string &caption,
         uint &expanded, int &db_type, std::string &parental_db,
         std::string &db_path, std::string &title, int &connected,
         classQuery &qdb
   )
{
   std::string 
         s_sql =  " SELECT"
                  "   tm.object_type,"
                  "   tm.parent_id,"
                  "   tm.parental_db,"
                  "   tm.db_type,"
                  "   tm.caption,"
                  "   tm.expanded,"
                  "   rd.db_path,"
                  "   rd.title,"
                  "   rd.connected"
                  " FROM"
                  "   t_tree_model AS tm"
                  " LEFT  JOIN"
                  "   t_registered_dbs AS rd"
                  " ON (rd.db_path = tm.parental_db) AND (rd.db_type = tm.db_type)"
                  " WHERE object_id = " + toString(object_id) + ""
                  " ORDER BY caption;";

   if (qdb.get_result(s_sql))
   {
         if (qdb.fetch_row())
         {
            object_type = qdb.getuval();
            parent_id = qdb.getval();
            parental_db = qdb.getstr();
            db_type = qdb.getuval();
            caption = qdb.getstr();
            expanded = qdb.getuval();
            db_path = qdb.getstr();
            title = qdb.getstr();
            connected = qdb.getval();
            qdb.free_result();
         } else {
            say.Warning("No object with id " + toString(object_id) + " found.");
            qdb.free_result();
            return CONST_ERROR;
         };
   } else {
         say.Error("Unable to read object kids.");
         return CONST_ERROR;      
   };
   
   return CONST_OK;

}

int
   classDBObjectTreeModel::objectInfo(
         int object_id, uint &object_type,
         int &parent_id, std::string &caption,
         uint &expanded, int &db_type, std::string &parental_db,
         classQuery &qdb
   )
{

   std::string s_sql = "SELECT object_type, parent_id, caption,"
                        " expanded, db_type, parental_db "
                        "FROM t_tree_model "
                       "WHERE object_id = " + toString(object_id) + " "
                       "ORDER BY caption";

   if (qdb.get_result(s_sql))
   {
         if (qdb.fetch_row())
         {
            object_type = qdb.getuval();
            parent_id = qdb.getval();
            caption = qdb.getstr();
            expanded = qdb.getuval();
            db_type = qdb.getuval();
            parental_db = qdb.getstr();
            qdb.free_result();
         } else {
            say.Warning("No object with id " + toString(object_id) + " found.");
            qdb.free_result();
            return CONST_ERROR;
         };
   } else {
         say.Error("Unable to read object kids.");
         return CONST_ERROR;      
   };
   
   return CONST_OK;

}

int
   classDBObjectTreeModel::objectTypeToID( const uint object_type, classQuery &qdb )
{

   std::string s_sql = "SELECT object_id FROM t_tree_model "
                       "WHERE object_type = " + toString(object_type) + " "
                       "ORDER BY object_id";

   int i_res = CONST_ERROR;

   if (qdb.get_result(s_sql))
   {
         if (qdb.fetch_row())
         {
            i_res = qdb.getval();
         } else {
            say.Warning("No object of type " + toString(object_type) + " found.");
         };
         qdb.free_result();
   } else {
         say.Error("Unable to read objects.");
   };
   
   return i_res;

}

int 
   classDBObjectTreeModel::registerDB(
      std::string s_path, std::string s_title,
      std::string &s_path_used, std::string &s_title_used,
      const int db_type, classQuery &qdb
   )
{
    int i_back = RDB_OK;
    std::string s_sql = "SELECT db_path, title FROM t_registered_dbs"
                        " WHERE db_path ='" + s_path + "'"
                        " OR title='" + s_title + "'";

    std::string s_t;
    std::string s_p;

    if (qdb.get_result(s_sql))
    {

         while (qdb.fetch_row())
         {
            s_p = qdb.getstr();
            s_t = qdb.getstr();
            i_back = ( s_p == s_path ? i_back | RDB_DB_REGISTERED : i_back);
            i_back = ( s_t == s_title ? i_back | RDB_TITLE_USED : i_back);
            if (s_p == s_path) { s_title_used = s_t; };
            if (s_t == s_title) { s_path_used = s_p; };
         };
         qdb.free_result();


     } else {
         say.Error("Unable to check registered dbs");
         return CONST_ERROR;      
     };

    if (i_back==RDB_OK) {
      if (!qdb.execute("INSERT INTO t_registered_dbs(db_path, db_type, title, connected)"
                  " VALUES('" + s_path + "', " + toString(db_type) + 
                  ",'" + s_title + "', " + toString(NOT_CONNECTED) + ")")
         )
      {
         say.Error("Unable to register the db");
         return CONST_ERROR;
      } else {
         say.Info("Database " + s_title + " has been registered");
         addObject(IT_DB, objectTypeToID(IT_ROOT_DB, qdb), s_title, EXPANDED, s_path, DT_DB, qdb);
      };
    }

   return i_back;
}

int 
   classDBObjectTreeModel::unregisterDB(
      std::string s_path, const int db_type, classQuery &qdb
   )
{

    if (!qdb.execute("BEGIN TRANSACTION")){
      say.Error("Transaction cannot be started");
      return CONST_ERROR;
    };

    std::string s_sql = "DELETE FROM t_registered_dbs"
                        " WHERE db_path ='" + s_path + "'"
                        " AND db_type=" + toString(db_type) + "";
    
    if (!qdb.execute(s_sql)){
      qdb.execute("END TRANSACTION");
      say.Error("Cannot delete the database from the registration list");
      return CONST_ERROR;
    };


    s_sql = "DELETE FROM t_tree_model"
                        " WHERE parental_db ='" + s_path + "'"
                        " AND db_type=" + toString(db_type) + "";

    if (!qdb.execute(s_sql)){
      qdb.execute("END TRANSACTION");
      say.Error("Cannot delete the database objects.");
      return CONST_ERROR;
    };

    if (!qdb.execute("COMMIT TRANSACTION")){
      say.Error("Transaction cannot be started");
      return CONST_ERROR;
    };

    return CONST_OK;
}

int
   classDBObjectTreeModel::dbConnected(
          int db_type, std::string db_path, classQuery &qdb
   )
{

   std::string s_sql = "SELECT connected "
                        "FROM t_registered_dbs "
                       "WHERE db_path = '" + db_path + "' "
                       "AND db_type = " + toString(db_type) + " ;";

   if (qdb.get_result(s_sql))
   {
         if (qdb.fetch_row())
         {
            int i_ret = qdb.getval();
            qdb.free_result();
            return i_ret;
         } else {
            qdb.free_result();
            say.Warning("No database '" + db_path + "' of type " + toString(db_type) + " has been found.");
            return CONST_ERROR;
         };
   } else {
         say.Warning("Unable to read the list of registered databases.");
         return CONST_ERROR;      
   };
   
}

int
   classDBObjectTreeModel::dbConnected(
      const int i_connected, int db_type, std::string db_path, classQuery &qdb
   )
{

   std::string s_sql = "UPDATE t_registered_dbs "
                        "SET connected = " + toString(i_connected) + " "
                       "WHERE db_path = '" + db_path + "' "
                       "AND db_type = " + toString(db_type) + " ;";

   if (!qdb.execute(s_sql)) {
         say.Error("Unable to change the db connected state.");
         return CONST_ERROR;      
   } else {
         return CONST_OK;
   };
   
}

/*!\mainpage sqool
   \author Marek Malevic
   \section intro_sec Introduction
      Sqool is a Qt4 application which aims to be an open sqlite3 database
      manager. It's beeing developed for GNU/Linux users under the GPL 2
      license (see the COPYING file).
   \section install_sec Installation
      To install the application from source code follow these steps:
      \subsection install_step1 Step 1: Prerequisites
         &nbsp;&nbsp;First, you need to have these headers installed:
         \li Qt4
         \li sqlite3
      \subsection install_step2 Step 2: Creating a Makefile
         &nbsp;&nbsp;To let the "make" tool do its job we have to prepare the<BR>
         &nbsp;&nbsp;Makefile file. Because the project stands on the Qt4 base<BR>
         &nbsp;&nbsp;we use the qtmake tool. So just do this:<BR>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CODE>./qmake -o Makefile sqool.pro</CODE>
      \subsection install_step3 Step 3: Compiling
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CODE>./make</CODE>
      \subsection install_step4 Step 4: Installing
         &nbsp;&nbsp;To install the application you need the root privileges.<BR>
         &nbsp;&nbsp;As root the instalation is started by:<BR>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CODE>./make install</CODE>
 */

#include    "../../include/core/appl.h"
#include    "../../include/core/stdoutput.h"
#include    "../../include/gui/gui.h"
#include    "../../include/core/glob.h"

/* Standard output engine initialization */
classStdoutput::classStdoutput say;

int main(int argc, char *argv[]){

   say.Info(APP_NAME);
   say.Info("version: " APP_VERSION);

   if (checkConfigurationPath() != CONST_OK) {
      say.Error("configuration directory not created");
      say.Advice("Please, check your '~/.config' write rights.");
      return CONST_ERROR;
   } else {
      say.Info("Configuration db is ready to be used.");
   }; 

   if (checkConfigurationStructure() != CONST_OK) {
      say.Error("configuration db not initialized");
      say.Advice("The structure of your configuration db ("
            + configurationFile() + ") might be broken. If your rights are OK removing the file could help.");
      return CONST_ERROR;
   } else {
      say.Info("Configuration db structure is OK.");
   };

   QApplication
      app(argc, argv);

   classMainForm
      ui_frmMain;

   ui_frmMain.show();

   return app.exec();
}

/*
    Author: Marek Malevic
    License:   GPL 2
*/

#include    "../../include/core/stdoutput.h"
#include    "../../include/core/appl.h"
#include    "../../include/core/glob.h"

#include    <QMessageBox>

classStdoutput::classStdoutput(VER_MODE v_mode)
{
   verbosity_mode = v_mode;
}


classStdoutput::~classStdoutput()
{
}

void
   classStdoutput::Error(std::string s_message, bool show_msgbox)
{
   if (verbosity_mode > V_MODE_MUTE){
      std::cerr << STRING_ERROR " " << s_message << std::endl;
   };
   if (show_msgbox){
      QMessageBox::critical(0, QString(APP_NAME " "  APP_VERSION), QString(s_message.c_str()));
   };
}

void
   classStdoutput::Warning(std::string s_message, bool show_msgbox)
{
   if (verbosity_mode > V_MODE_ERRORS){
      std::clog << STRING_WARNING << " " << s_message << std::endl;
   };
   if (show_msgbox){
      QMessageBox::warning(0, QString(APP_NAME " "  APP_VERSION), QString(s_message.c_str()));
   };
}

void
   classStdoutput::Info(std::string s_message, bool show_msgbox)
{
   if (verbosity_mode > V_MODE_ADVICE){
      std::cout << STRING_INFO <<  " " << s_message << std::endl;
   };
   if (show_msgbox){
      QMessageBox::information(0, QString(APP_NAME " "  APP_VERSION), QString(s_message.c_str()));
   };
}

void
   classStdoutput::Advice(std::string s_message, bool show_msgbox)
{
   if (verbosity_mode > V_MODE_WARNINGS){
      std::cout << STRING_ADVICE <<  " " << s_message << std::endl;
   };
   if (show_msgbox){
      QMessageBox::information(0, QString(APP_NAME " "  APP_VERSION), QString(s_message.c_str()));
   };
}


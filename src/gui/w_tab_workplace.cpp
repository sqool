/*
    Database Object Tree class 
    Author: Marek Malevic
    License:   GPL 2
*/

#include       "../../include/gui/w_tab_workplace.h"
#include       "../../include/core/stdoutput.h"

/* joins the standard output machine */
extern         classStdoutput::classStdoutput say;

classTabWorkplace::classTabWorkplace()
{
   
   say.Info("Tabbed Workplace has been initialized.");

}

classTabWorkplace::~classTabWorkplace()
{
   say.Info("Tabbed Workplace has been destroyed.");
}


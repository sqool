/*
    Database Object Tree class 
    Author: Marek Malevic
    License:   GPL 2
*/

#include       <QMessageBox>

#include       "../../include/gui/w_object_tree.h"
#include       "../../include/core/appl.h"
#include       "../../include/core/database.h"
#include       "../../include/core/query.h"
#include       "../../include/core/stdoutput.h"
#include       "../../include/core/glob.h"
#include       "../../include/core/utils.h"

/* joins the standard output machine */
extern         classStdoutput::classStdoutput say;

classDBObjectTree::classDBObjectTree()
{
  // QStringList headers("");
   setHeaderLabels((QStringList) "");
  // setColumnCount(2);

   model = new classDBObjectTreeModel();
   if (redrawTree() != CONST_OK) {
      say.Error("Unable to redraw the object tree.");
   };
   
   say.Info("DB Object Tree has been initialized.");

}

classDBObjectTree::~classDBObjectTree()
{
   delete model;
   say.Info("DB Object Tree has been destroyed.");
}

int
   classDBObjectTree::drawObject(int object_id, QTreeWidgetItem *parent, QTreeWidgetItem *&kid, classQuery &qdb)
{
   
   uint  object_type;
   int   parent_id;
   std::string
         caption;
   std::string
         parental_db;
   uint  expanded;
   int   db_type;

   if (model->objectInfo(object_id, object_type, parent_id, caption, expanded, db_type, parental_db, qdb) != CONST_OK) {
      say.Error("Cannot get the object info.");      
      return CONST_ERROR;
   } else {
      say.Info("Object id: '" + toString(object_id) +
         "' object type: '" + toString(object_type) +
         "' object parent id: '" + toString(parent_id) +
         "' caption: '" + caption + "'" +
         "' db_type: '" + toString(db_type) + "'"
   );
   };
   
   if (parent_id == -1) { kid = new QTreeWidgetItem(this, object_type); }
      else {  kid = new QTreeWidgetItem(parent, object_type);  };
   switch (object_type) {
      case IT_ROOT_DB: /* registered relational databases root*/
         kid->setIcon(0, QIcon(":object_type_registered_relational_dbs.png"));
         kid->setText(0, tr("databases"));
         kid->setText(1, QString(toString(object_id).c_str()));
         kid->setExpanded(expanded);
         break;

      case IT_DB: /* registered relational database*/
         kid->setIcon(0,
                        (
                           (model->dbConnected(db_type, parental_db, qdb)!=NOT_CONNECTED) ?
                           QIcon(":object_type_registered_db_connected.png") : 
                           QIcon(":object_type_registered_db_disconnected.png")
                        )
                     );
         kid->setText(0, QString(caption.c_str()));
         kid->setText(1, QString(toString(object_id).c_str()));
         kid->setExpanded(expanded);
         break;

      default: /* unknown object type */
         say.Warning("Unknown object type " + toString(object_type));
         kid->setIcon(0, QIcon(":object_type_unknown.png"));
         kid->setText(0, QString(caption.c_str()));
         kid->setText(1, QString(toString(object_id).c_str()));
         kid->setExpanded(expanded);
         break;
   };
   
   return CONST_OK;
}

int 
   classDBObjectTree::drawKids(int object_id, QTreeWidgetItem *parent, classQuery &qdb)
{
   std::list<int> li;

   model->getKids(object_id, li, qdb);
   for (std::list<int>::iterator kid = li.begin(); kid != li.end(); ++kid)
   {
      QTreeWidgetItem *new_kid;
      int ret = drawObject(*kid, parent, new_kid, qdb);
      if (ret!=CONST_OK)
      {
         say.Error("Object #" + toString(*kid) + " drawing failed.");
         QMessageBox::critical(this, APP_NAME " " APP_VERSION,
                   tr("Error while drawing the object tree model."),
                   QMessageBox::Ok);
      } else {
         drawKids(*kid, new_kid, qdb);
      };
   }
   return CONST_OK;   
};

int
   classDBObjectTree::redrawTree()
{
   /* first read all root elements (parent -1 is root)*/
   say.Info("Redrawing the object tree.");
   int i_id = selectedObjectID();
   say.Info("Caused by " + toString(i_id));
   setCurrentItem(NULL);
   clear();
   classDatabase cdb(configurationFile());
   if (!cdb.Connected())
   {
      say.Error("Unable to connect the configuration db for redrawing the object tree.");
      return CONST_ERROR;
   };

   classQuery qdb(cdb);

   int i_ret = drawKids(-1, NULL, qdb);   

   if ((i_id != CONST_ERROR) && (i_ret != CONST_ERROR)) {
      QTreeWidgetItemIterator it(this);
      while (*it) {
      //   say.Info((*it)->text(1).toStdString());
         if ((*it)->text(1).toInt() == i_id)
            setCurrentItem(*it);
            setItemSelected(*it, true);
         ++it;
     }
   }

   return i_ret;

}

int            
   classDBObjectTree::selectedObjectID()
{
   if (currentItem()==NULL) {return CONST_ERROR;};
   return  selectedItems()[0]->text(1).toInt();
}

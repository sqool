
#include       "../../include/gui/frm_reg.h"
#include       "../../include/core/glob.h"
#include       "../../include/core/appl.h"
#include       "../../include/gui/frm_main.h"
#include       "../../include/core/stdoutput.h"

/* joins the standard output machine */
extern         classStdoutput::classStdoutput say;

classRegForm::classRegForm(QWidget *parent)
    : QWidget(parent)
{
 

    Qt::WindowFlags	flags = 0;

    setFixedSize(FRM_REG_WIDTH, FRM_REG_HEIGHT); 
    setWindowTitle(tr("Register new database"));
 
    setWindowFlags(Qt::Dialog);
    setAttribute(Qt::WA_DeleteOnClose);
    move(
            parent->x() + (parent->width()/2) - (width()/2),
            parent->y() + (parent->height()/2) - (height()/2)        
        );
            
    path_label = new QLabel(
                                tr( "Select an SQLite3 database file to be registered. "
                                    "If the database does not exist, "
                                    "it will be created later while connecting."
                                  ),
                                this
                           );
    path_label->setWordWrap(true);
    path_label->resize(width()-FRM_REG_SIZE_CONSTS_LEFT*2, FRM_REG_SIZE_CONSTS_TOP+20);
    path_label->move(FRM_REG_SIZE_CONSTS_LEFT, FRM_REG_SIZE_CONSTS_TOP);
    path_label->setAlignment(Qt::AlignBottom);
    
    db_path = new QLineEdit(this);
    db_path->move(
         FRM_REG_SIZE_CONSTS_LEFT,
         FRM_REG_SIZE_CONSTS_TOP + path_label->y() + path_label->height()
        );
    db_path->resize(width()-115,25);
    
    title_label = new QLabel(
                                tr( "The title used by the application instead of "
                                    "the whole db path."),
                                this
                           );
    title_label->setWordWrap(true);
    title_label->resize(width()-FRM_REG_SIZE_CONSTS_LEFT*2, FRM_REG_SIZE_CONSTS_TOP+10);
    title_label->move(FRM_REG_SIZE_CONSTS_LEFT,
            db_path->y() + db_path->height() + 2*FRM_REG_SIZE_CONSTS_TOP
         );
    title_label->setAlignment(Qt::AlignBottom);
    
    db_title = new QLineEdit(this);
    db_title->move(
            FRM_REG_SIZE_CONSTS_LEFT,
            FRM_REG_SIZE_CONSTS_TOP + title_label->y() + title_label->height()
         );
    db_title->resize( 200, 25 );
    
    button_select_db = new QPushButton(tr("o&pen"),this);
    button_select_db->move(FRM_REG_SIZE_CONSTS_LEFT+db_path->width()+
               FRM_REG_SIZE_CONSTS_LEFT-5,
               db_path->y()
         );
    button_select_db->resize(width()-FRM_REG_SIZE_CONSTS_LEFT*2 -
            (FRM_REG_SIZE_CONSTS_LEFT-5) - db_path->width(),
            db_path->height()
         );
    button_select_db->setAutoDefault(true);
        
    button_cancel = new QPushButton(tr("&cancel"),this);
    button_cancel->resize(FRM_REG_SIZE_CONSTS_BUTTON_WIDTH,db_path->height());
    button_cancel->move(width() - FRM_REG_SIZE_CONSTS_BUTTON_WIDTH
            - FRM_REG_SIZE_CONSTS_LEFT,
            height() - FRM_REG_SIZE_CONSTS_TOP - button_cancel->height()
         );
    button_cancel->setAutoDefault(true);
    
    button_ok = new QPushButton(tr("&OK"),this);
    button_ok->resize(FRM_REG_SIZE_CONSTS_BUTTON_WIDTH, db_path->height());
    button_ok->move(button_cancel->x() - FRM_REG_SIZE_CONSTS_LEFT - button_ok->width(),
            button_cancel->y()
         );
    button_ok->setEnabled(false);
    button_ok->setAutoDefault(true);
    
    file_dialog = new QFileDialog(this);
    file_dialog->setFileMode(QFileDialog::AnyFile);
    file_dialog->setAcceptMode(QFileDialog::AcceptOpen);
    
    setTabOrder(button_ok, button_cancel);
    setTabOrder(button_cancel, db_path);
    setTabOrder(db_path,button_select_db);
    setTabOrder(button_select_db, db_title);
    
    say.Info("Connecting signals to slots");
    connect(button_select_db,SIGNAL(clicked()),file_dialog,SLOT(exec()));
    connect(button_cancel, SIGNAL(clicked()),this,SLOT(close()));
    connect(button_ok, SIGNAL(clicked()),this,SLOT(regDBOK()));
    connect(db_path, SIGNAL(textChanged(QString)),this,SLOT(checkButtons()));
    connect(db_title, SIGNAL(textChanged(QString)),this,SLOT(checkButtons()));
    
    say.Info("Connecting the file dialog");
    connectFileDialog();

    say.Info("The 'register db' dialog has been created.");
}

void classRegForm::connectFileDialog()
{
    connect(file_dialog,SIGNAL(accepted()),this,SLOT(fillPath()));
}

void classRegForm::fillPath()
{
    QStringList list = file_dialog->selectedFiles();
    QStringList::Iterator it = list.begin();

    QString s = *it;
    QFileInfo fi(s);
    
    db_path->setText(s);
    db_title->setText(fi.baseName().toUpper());
    button_ok->setFocus();
}

void classRegForm::checkButtons()
{
    bool b = true;
    b &= !db_path->text().isEmpty();
    b &= !db_title->text().isEmpty();
    
    button_ok->setEnabled(b);
}

void classRegForm::regDBOK()
{
    QString s_path = db_path->text();
    QString s_title = db_title->text();
    std::string s_used_path;
    std::string s_used_title;

    classDatabase cdb(configurationFile());
    if (!cdb.Connected()) {
       say.Error("Error occured while connectiong to the configuration database.", TRUE);
       return;
    };

    classQuery qdb(cdb);
    int i = ((classMainForm*)parentWidget())->dbOTree->model->registerDB(
               s_path.toStdString(),
               s_title.toStdString(),
               s_used_path,
               s_used_title,
               DT_DB,
               qdb
            );
    switch(i){
        case(RDB_DB_REGISTERED | RDB_TITLE_USED):{
            if ((s_used_path == s_path.toStdString()) && (s_used_title == s_title.toStdString()))
            {
                QMessageBox::warning(this, APP_NAME,tr("This database has already been registered. You cannot register it again."));
            } else {
                QString s_arg1(s_used_title.c_str());
                QString s_arg2(s_used_path.c_str());
                QMessageBox::warning(this, APP_NAME,tr("This database has already been registered with the title '%1'."
                        " You cannot register it again.\n\nAlso the title you've selected has already been used - "
                                "it's a titleQWidget *parent of the db '%2'.").arg(s_arg1).arg(s_arg2));
            };
            break;}
        case(RDB_TITLE_USED):{
            QString s_arg1(s_used_path.c_str());
            QMessageBox::warning(this, APP_NAME,tr("The title you've selected has already been used - "
                            "it's a title of the db '%1'.\n\nPlease, choose another title.").arg(s_arg1));
            break;}        
        case(RDB_DB_REGISTERED):{
            QString s_arg1(s_used_title.c_str());
            QMessageBox::warning(this, APP_NAME,tr("This database has already been registered with the title '%1'."
                    "\n\nYou cannot register this db again.").arg(s_arg1));
            break;}
        case(RDB_OK):{
            ((classMainForm*)parentWidget())->dbOTree->redrawTree();
            close();
            break;}
    }


}

classRegForm::~classRegForm()
{
   say.Info("Destroying the 'register db' dialog");
}



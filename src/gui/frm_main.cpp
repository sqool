/*
    Application main window code (GUI) 
    Author: Marek Malevic
    License:   GPL 2
*/

#include       "../../include/gui/frm_main.h"
#include       "../../include/core/appl.h"
#include       "../../include/core/actions.h"
#include       "../../include/core/stdoutput.h"
#include       "../../include/core/query.h"
#include       "../../include/core/database.h"
#include       "../../include/core/glob.h"
#include       "../../include/core/utils.h"

/* joins the standard output machine */
extern         classStdoutput::classStdoutput say;

classMainForm::classMainForm()
{
   // window geometry
   setMinimumSize(FRMMAIN_UI_WMINW, FRMMAIN_UI_WMINH);
   setWindowTitle(tr("%1 version %2").arg(APP_NAME).arg(APP_VERSION));
   setWindowFlags(Qt::Window);
   resize(configGetValue("MainWindowWidth",800),configGetValue("MainWindowHeight",600));
   setWindowStateInt(configGetValue("MainWindowState",0));

   // central widget
   tabWorkplace = new classTabWorkplace();
   setCentralWidget(tabWorkplace);

   //database objects tree
   dbOTree = new classDBObjectTree();

   //docking
   QDockWidget *dock = new QDockWidget("", this);
   dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
   dock->setFeatures(QDockWidget::DockWidgetMovable);

   dock->setWidget(dbOTree);

   addDockWidget(Qt::LeftDockWidgetArea, dock);

   //actions
   createActions();
   checkActions();

   //menus
   createMenus();


/*
    TabWorkplace = new QTabWidget(this);
    QPushButton *CornerButton = new QPushButton("", TabWorkplace);
    CornerButton->setIcon(QIcon(":/icons/tab_remove.png"));
    CornerButton->setFlat(true);
    TabWorkplace->setCornerWidget(CornerButton);

            
    setCentralWidget(TabWorkplace);
    
    connect(TabWorkplace, SIGNAL(currentChanged(int)), this, SLOT(slotTabChanged()));
    connect(CornerButton, SIGNAL(clicked()), this, SLOT(slotCloseCurrentTab()));

    CreateActions();
    CreateMenus();
    CreateToolBars();
    CreateDockWindows();
*/
   say.Info("Main window has been initialized.");

}

classMainForm::~classMainForm()
{

    delete dbOTree;
    delete tabWorkplace;

    configSetValue("MainWindowWidth", width());
    configSetValue("MainWindowHeight", height());
    
    unsigned short int ws = getWindowStateInt();
    //we do not want to start minimized, do we? so, no 0x01
    ws &= 0x6;
    configSetValue("MainWindowState", ws);

    say.Info("Good bye!");
}

unsigned short int classMainForm::getWindowStateInt()
{
    unsigned short int i_winstate = 0x0;
    const Qt::WindowStates    ws = windowState();
    
    if (ws & Qt::WindowMinimized)
    {
        i_winstate |= 0x1;
    };    
    
    if (ws & Qt::WindowMaximized)
    {
        i_winstate |= 0x2;
    };
    
    if (ws & Qt::WindowFullScreen)
    {
        i_winstate |= 0x4; 
    };
    
    return i_winstate;
}

void classMainForm::setWindowStateInt(const int i_winstate)
{
    Qt::WindowStates    ws = Qt::WindowNoState;
    
    if (i_winstate & 0x1)
    {
        ws |= Qt::WindowMinimized;
    };    
    
    if (i_winstate & 0x2)
    {
        ws |= Qt::WindowMaximized;
    };
    
    if (i_winstate & 0x4)
    {
        ws |= Qt::WindowFullScreen;
    };
    setWindowState(ws);
}

void classMainForm::createMenus()
{

   say.Info("Creating main menu");
   fileMenu = menuBar()->addMenu(tr("&File"));
   fileMenu->addAction(actExit);

   databaseMenu = menuBar()->addMenu(tr("&Database"));
   databaseMenu->addAction(actRegisterDB);
   databaseMenu->addAction(actUnRegisterDB);
   databaseMenu->addSeparator();
   databaseMenu->addAction(actConnectDB);
   databaseMenu->addAction(actDisConnectDB);

}

void classMainForm::createActions()
{
    actExit = new QAction(tr("E&xit"), this);
    actExit->setShortcut(tr("Ctrl+Q"));
    actExit->setStatusTip(tr("Exit the application"));
    actExit->setIcon(QIcon(":action_exit.png"));
    connect(actExit, SIGNAL(triggered()), this, SLOT(close()));

    actRegisterDB = new QAction(tr("R&egister"), this);
    actRegisterDB->setShortcut(tr("Ctrl+R"));
    actRegisterDB->setStatusTip(tr("Register the database"));
    actRegisterDB->setIcon(QIcon(":action_register_db.png"));
    connect(actRegisterDB, SIGNAL(triggered()), this, SLOT(registerDb()));

    actUnRegisterDB = new QAction(tr("&Unregister"), this);
    actUnRegisterDB->setStatusTip(tr("Unregister the database"));
    actUnRegisterDB->setIcon(QIcon(":action_unregister_db.png"));
    connect(actUnRegisterDB, SIGNAL(triggered()), this, SLOT(unregisterDb()));

    actConnectDB = new QAction(tr("&Connect"), this);
    actConnectDB->setShortcut(tr("Ctrl+P"));
    actConnectDB->setStatusTip(tr("Connect the database"));
    actConnectDB->setIcon(QIcon(":action_connect_db.png"));
    connect(actConnectDB, SIGNAL(triggered()), this, SLOT(connectDb()));

    actDisConnectDB = new QAction(tr("&Disconnect"), this);
    actDisConnectDB->setShortcut(tr("Ctrl+D"));
    actDisConnectDB->setStatusTip(tr("Disconnect the database"));
    actDisConnectDB->setIcon(QIcon(":action_disconnect_db.png"));
    connect(actDisConnectDB, SIGNAL(triggered()), this, SLOT(disconnectDb()));

    connect(dbOTree, SIGNAL(itemSelectionChanged()), this, SLOT(checkActions()));
}

void classMainForm::checkActions()
{

   uint object_type;
   int parent_id;
   std::string caption;
   uint expanded;
   int db_type;
   std::string parental_db;
   std::string db_path;
   std::string title;
   int connected;
   int o_id = 0;

   bool bAnyDBObjectSelected = (dbOTree->currentItem() != NULL);

   if (bAnyDBObjectSelected) {
      classDatabase cdb(configurationFile());
      if (!cdb.Connected())
      {
         say.Error("Unable to connect to the configuration db for reading the object tree information.", TRUE);
         bAnyDBObjectSelected = FALSE;   
      } else {
         classQuery qdb(cdb);
         o_id = dbOTree->selectedObjectID();
         if (dbOTree->model->objectInfo(o_id, object_type, parent_id, caption,
                        expanded, db_type, parental_db, db_path,
                        title, connected, qdb) == CONST_ERROR)
         {            
            if (o_id != CONST_ERROR) {
               say.Error("Unable to check actions.",TRUE);
            };
            bAnyDBObjectSelected = FALSE;   
         };
      };
   };

/*
   say.Info("any selected: " + toString(bAnyDBObjectSelected) +   
            "    db selected: " + toString(bDBSelected) +
             "    object id: " + toString(o_id) +
             "    object type: " + toString(object_type) +
             "    caption: " + caption 
            );
*/
   
   actExit->setEnabled(TRUE);
   actRegisterDB->setEnabled(TRUE);
   actUnRegisterDB->setEnabled(bAnyDBObjectSelected && (object_type == IT_DB));
   actConnectDB->setEnabled(bAnyDBObjectSelected && (object_type == IT_DB) && (connected == NOT_CONNECTED));
   actDisConnectDB->setEnabled(bAnyDBObjectSelected && (object_type == IT_DB) && (connected == CONNECTED));

}

void classMainForm::registerDb()
{
   frmReg = new classRegForm(this);
   frmReg->setWindowModality(Qt::ApplicationModal);
   frmReg->show();
}

void classMainForm::unregisterDb()
{

   uint object_type;
   int  parent_id;
   std::string caption;
   uint expanded;
   int  db_type;
   std::string parental_db;

   classDatabase cdb(configurationFile());
   if (!cdb.Connected())
   {
      say.Error("Unable to connect the configuration db for reading the object tree.");
      return;
   };

   classQuery qdb(cdb);

   dbOTree->model->objectInfo(dbOTree->selectedObjectID(),object_type,
         parent_id, caption,
         expanded, db_type, parental_db,
         qdb);

   if (
      QMessageBox::question(0,APP_NAME,
         tr("Do you really want to unregister the database '%1' (%2)?").arg(
                  QString(caption.c_str())
               ).arg(
                  QString(parental_db.c_str()))
            , QMessageBox::Yes | QMessageBox::No, QMessageBox::No) ==  QMessageBox::Yes 
      ) {
      if (dbOTree->model->unregisterDB(parental_db, db_type, qdb)==CONST_OK)
      {
         dbOTree->redrawTree();
         say.Info("Database unregistered.");
         checkActions();
      };
    };
}


void classMainForm::connectDb()
{
  uint object_type;
   int  parent_id;
   std::string caption;
   uint expanded;
   int  db_type;
   std::string parental_db;

   classDatabase cdb(configurationFile());
   if (!cdb.Connected())
   {
      say.Error("Unable to connect the configuration db for reading the object tree.");
      return;
   };

   classQuery qdb(cdb);

   dbOTree->model->objectInfo(dbOTree->selectedObjectID(),object_type,
         parent_id, caption,
         expanded, db_type, parental_db,
         qdb);

      if (dbOTree->model->dbConnected(CONNECTED,db_type,parental_db, qdb)==CONST_OK)
      {
         dbOTree->redrawTree();
         say.Info("Database " + parental_db + " connected.");
         checkActions();
      } else {
         say.Error("The database could not be connected.");
      };
}

void classMainForm::disconnectDb()
{
  uint object_type;
   int  parent_id;
   std::string caption;
   uint expanded;
   int  db_type;
   std::string parental_db;

   classDatabase cdb(configurationFile());
   if (!cdb.Connected())
   {
      say.Error("Unable to disconnect the configuration db for reading the object tree.");
      return;
   };

   classQuery qdb(cdb);

   dbOTree->model->objectInfo(dbOTree->selectedObjectID(),object_type,
         parent_id, caption,
         expanded, db_type, parental_db,
         qdb);

      if (dbOTree->model->dbConnected(NOT_CONNECTED,db_type,parental_db, qdb)==CONST_OK)
      {
         dbOTree->redrawTree();
         say.Info("Database " + parental_db + " disconnected.");
         checkActions();
      } else {
         say.Error("The database could not be disconnected.");
      };
}

/*
void CMainForm::CreateDockWindows()
{
    QDockWidget *dock = new QDockWidget("", this);
    dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dock->setFeatures(QDockWidget::DockWidgetMovable);
        
    dbTree = new QTreeWidget(dock);
    dbTree->setColumnCount(2);
    dbTree->setColumnHidden(1, true);
    dbTree->setContextMenuPolicy(Qt::CustomContextMenu); 
    QStringList headers;
    headers << "";
    dbTree->setHeaderLabels(headers);
    
    dock->setWidget(dbTree);
    
    QTreeWidgetItem *rdb = new QTreeWidgetItem(dbTree);
    rdb->setText(0, tr("registered databases"));
    rdb->setText(1, QVariant(IT_CATEGORY_DB).toString());
    rdb->setIcon(0, QIcon(":icons/registered_dbs.png"));
    
    connect(this, SIGNAL(signalReloadRegisteredDBs()), this, SLOT(slotReloadRegisteredDBs()));
    connect(dbTree, SIGNAL(itemSelectionChanged()),this,SLOT(slotTreeWidgetClicked()));
    connect(dbTree, SIGNAL(customContextMenuRequested(const QPoint)), this, SLOT(slotTreeWidgetRightClicked(const QPoint)));
    connect(dbTree, SIGNAL(itemDoubleClicked( QTreeWidgetItem *, int )),this,SLOT(slotConnectDBonDblClick(QTreeWidgetItem*, int)));
    
    emit signalReloadRegisteredDBs();
    
    addDockWidget(Qt::LeftDockWidgetArea, dock);    
}
*/
